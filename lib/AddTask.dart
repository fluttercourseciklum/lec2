import 'package:flutter/material.dart';

final newItemController = TextEditingController();

class AddTask extends StatefulWidget {
  const AddTask({Key key, this.parentAction}) : super(key: key);

  final ValueChanged<String> parentAction;

  @override
  State<StatefulWidget> createState() {
    return AddTaskState();
  }
}

class AddTaskState extends State<AddTask> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('Add new task'),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Form(
            key: _formKey,
            child: TextFormField(
              autofocus: true,
              controller: newItemController,
              decoration: const InputDecoration(
                labelText: 'New item text',
              ),
              validator: (value) {
                if (value.isEmpty) {
                  return 'Please enter some text';
                } else if (value.length < 3) {
                  return 'Minimal length is 3';
                }
                return null;
              },
            ),
          )
        ],
      ),
      actions: <Widget>[
        FlatButton(
          child: const Text('Cancel'),
          onPressed: () {
            newItemController.clear();
            Navigator.pop(context);
          },
        ),
        FlatButton(
          child: const Text('Ok'),
          onPressed: () {
            if (_formKey.currentState.validate()) {
              widget.parentAction(newItemController.text);
              Navigator.pop(context);
              newItemController.clear();
            }
          },
        ),
      ],
    );
  }
}
