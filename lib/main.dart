import 'package:flutter/material.dart';
import 'package:todo_list/AddTask.dart';
import 'package:todo_list/TodoList.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: MyHomePage(title: 'Todo List'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final items = <String>[];

  dynamic _addToList(String text) {
    setState(() => items.add(text));
  }

  dynamic _dropItem(int index) {
    setState(() => items.removeAt(index));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(widget.title),
          ),
        ),
      ),
      body: TodoList(
        items: items,
        parentAction: _dropItem,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => showDialog<void>(
            context: context,
            barrierDismissible: false,
            builder: (BuildContext context) => AddTask(
                  parentAction: _addToList,
                )),
        tooltip: 'Add new',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
