import 'package:flutter/material.dart';

class TodoList extends StatefulWidget {
  const TodoList({Key key, this.items, this.parentAction}) : super(key: key);

  final List<String> items;
  final ValueChanged<int> parentAction;

  @override
  State<StatefulWidget> createState() {
    return TodoListState();
  }
}

class TodoListState extends State<TodoList> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.items.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            leading: Text(index.toString()),
            title: Text(widget.items[index]),
            trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  widget.parentAction(index);
                }),
          );
        });
  }
}
